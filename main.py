import numpy as np
import pandas as pd
import yfinance as yf
import matplotlib.pyplot as plt
import datetime
from scipy.signal import argrelextrema

def getData():
    aapl = yf.Ticker('AAPL').history(period = '10y')
    return aapl

def draw(data, patterns):
    data.plot()
    plt.xlabel("Date")
    plt.ylabel("Close")
    plt.title("AAPL price data")
    # mark extrema with colors, Orange:maxima, Blue: minima
    plt.scatter(patterns['HS'].index.values, patterns['HS'].values, color='Orange', alpha=.5)
    plt.scatter(patterns['IHS'].index.values, patterns['IHS'].values, color='Blue', alpha=.5)
    plt.show()

def findExtrema(data, smoothing, window):
    # get MA line with (smoothing:days)
    smooth = data.rolling(window=smoothing).mean().dropna()
    # get local extrema from MA line
    localMax = argrelextrema(smooth.values, np.greater)[0]
    localMin = argrelextrema(smooth.values, np.less)[0]
    # use these extrema to find extrema in the window
    # 以平滑線上的極值之 index 於原始資料中, 往前往後找 window 內之極值, 是為極值
    localMaxData = []
    for i in localMax:
        if (i>window) and (i<len(data)-window):
            localMaxData.append(data.iloc[i-window:i+window].idxmax())
    localMinData = []
    for i in localMin:
        if (i>window) and (i<len(data)-window):
            localMinData.append(data.iloc[i-window:i+window].idxmin())
    maxima = pd.DataFrame(data.loc[localMaxData])
    minima = pd.DataFrame(data.loc[localMinData])
    extrema = pd.concat([maxima, minima]).sort_index().drop_duplicates()
    return extrema

def findPatterns(extrema):
    # 在極值中依照公式尋找符合 Head shoulder patterns 的數值組
    count = 0
    # HS
    hs = []
    for i in range(5, len(extrema)):
        window = extrema.iloc[i-5:i]
        a, b, c, d, e = window.iloc[0:5].values
        if a>b and c>a and c>e and c>d and e>d and abs(b-d)<=np.mean([b,d])*0.015 and abs(a-e)<=np.mean([b,d])*0.015:
            hs.append(window)
            count += 1
    hs = pd.concat(hs).drop_duplicates()
    # IHS
    ihs = []
    for i in range(5, len(extrema)):
        window = extrema.iloc[i-5:i]
        a, b, c, d, e = window.iloc[0:5].values
        if a<b and c<a and c<e and c<d and e<d and abs(b-d)<=np.mean([b,d])*0.015 and abs(a-e)<=np.mean([b,d])*0.015:
            ihs.append(window)
            count += 1
    ihs = pd.concat(ihs).drop_duplicates()
    patterns = [hs, ihs]
    return patterns, count

def main():
    data = getData()
    # get data smoothing: 1, window: 1
    extrema = findExtrema(data['Close'], 1, 1)
    patterns, count = findPatterns(extrema)
    # 將有在 hs 的設為 1 有在 ihs 的設為 -1 其餘為 0
    data['trend']=0
    data.loc[patterns[0].index, 'trend']=1
    data.loc[patterns[1].index, 'trend']=-1
    if count == 0:
        print('No patterns found')
    else:
        # draw(data['Close'], patterns)
        print(count, 'patterns found.')
        print(data['trend'].values)
        print('done')

if __name__ == "__main__":
    main()
